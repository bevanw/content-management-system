from django.db import models
from django.utils import timezone

# Create your models here.
class Post(models.Model):
	title = models.CharField(max_length=100)
	post = models.TextField(max_length=1000)
	pub_date = models.DateTimeField('date published')
	def __unicode__(self):
		return self.title
	def was_published_recently(self):
		return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
	
class Comment(models.Model):
	post = models.ForeignKey(Post)
	comment_email = models.EmailField(max_length=100)
	comment_name = models.CharField(max_length=100)
	comment_text = models.TextField(max_length=1000)
	comment_date = models.DateTimeField('time commented')
	def __unicode__(self):
		return self.comment_text