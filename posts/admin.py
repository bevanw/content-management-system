from django.contrib import admin
from posts.models import Post
from posts.models import Comment

# Register your models here.

class CommentsInline(admin.TabularInline):
	model = Comment
	extra = 1

class PostAdmin(admin.ModelAdmin):
	fieldsets = [
		('Post information', {'fields': ['title','post']}),
		('Date information', {'fields':['pub_date']}),
	]
	inlines = [CommentsInline]
	list_display = ('title','post','pub_date')
	
admin.site.register(Post,PostAdmin)