from django.shortcuts import render, get_object_or_404
from posts.models import Post, Comment
from django.utils import timezone

# Create your views here.
from django.http import HttpResponse
from django.template import RequestContext, loader

def index(request):
	latest_post_list = Post.objects.order_by('-pub_date')
	comments_list = Comment.objects.order_by('comment_date')
	context = {'latest_post_list':latest_post_list,'comments_list':comments_list}
	return render(request,'posts/index.html',context)
	
def comment(request,post_id):
	#return HttpResponse("You are commenting on %s." % post_id)
	try:
		text = request.POST['comment_text']
		date = timezone.now()
		email = request.POST['email']
		name = request.POST['name']
	except (KeyError):
		return HttpResponse("Error")
	else:
		p = get_object_or_404(Post,pk=post_id)
		new_comment = Comment(post = p,comment_email=email,comment_date=date,comment_text=text,comment_name=name)
		new_comment.save()
		return HttpResponse("Comment: %s, Date: %s, E-mail: %s, Name: %s" % (text, date, email, name))