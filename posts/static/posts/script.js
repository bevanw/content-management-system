$(document).ready(function ()
{
	$(".show_make_comment").click(function ()
	{
		$(this).next(".makeComment").toggle();
		return false;
	});
	$(".show_comments").click(function ()
	{
		$(this).next(".comments").toggle();
		return false;
	});
	
	$(".submit_comment").submit(function(event)
	{
		event.preventDefault();
		var theURL = $(this).attr("action")+"?"+$(this).serialize();
		var theData = $(this).serialize();
		var csrftoken = $('[name="csrfmiddlewaretoken"]').val();
		$.ajax(
		{
		  url: theURL,
		  context: document.body,
		  type: "POST",
		  data: theData,
		  beforeSend: function(xhr, settings) 
		  {
			xhr.setRequestHeader("X-CSRFToken", csrftoken);
			
		  }
		}).done(function(response) 
		{
			//alert(response);
			if (response != "Error")
			{
				alert("Comment submitted");
				location.reload();
			}
		});
		return false;
	});
});

