from django.conf.urls import patterns, url

from posts import views

urlpatterns = patterns('',
	#Polls:
	url(r'^$',views.index, name="index"),
	
	url(r'^(?P<post_id>[0-9]+)/comment/$',views.comment, name="comment"),
)